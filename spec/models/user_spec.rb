require 'rails_helper'

RSpec.describe User, type: :model do

  it { should have_many(:posts).dependent(:destroy) }
  it { should have_many(:applications).dependent(:destroy) }

  # describe '#after_initialization' do
  #
  #   it 'job_admin value should be false by default' do
  #
  #     user = User.new(email: 'test@gmail.com', password: 'qwerty', password_confirmation: 'qwerty')
  #
  #     expect(user.job_admin).to be_falsey
  #
  #   end
  #
  # end

  #
  # describe 'validations' do
  #
  #   it 'should be valid with valid attributes' do
  #
  #     user = User.new(email: 'test@gmail.com', password: '123456', password_confirmation: '123456', job_admin: false)
  #
  #     expect(user).to be_valid
  #
  #   end
  #
  #
  #   it 'should be invalid without a password' do
  #
  #     user = User.new(email: 'test@gmail.com', password: nil, password_confirmation: nil)
  #
  #     expect(user).to_not be_valid
  #
  #   end
  #
  #
  #   it 'should be invalid with wrong password_confirmation' do
  #
  #     user = User.new(email: 'test@gmail.com', password: 'qwerty', password_confirmation: 'asdfghj')
  #
  #     expect(user).to_not be_valid
  #
  #   end
  #
  #
  # end


end