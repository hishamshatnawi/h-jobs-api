FactoryBot.define do
  factory :post do
    title { Faker::Lorem.word }
    description { Faker::StarWars.character }
    user
  end
end