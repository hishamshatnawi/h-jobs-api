class PostsController < ApplicationController
  include Response
  skip_before_action :verify_authenticity_token
  before_action :authenticate_request!
  before_action :set_post, only: [:show, :update, :destroy]

  # GET /posts
  def index
    @posts = Post.all
    if @posts.nil? || @posts.blank?
      msg = {:message => "There is no post record"}
      render :json => msg, :status => 201
      return
    end
    render json: @posts

  end

  # POST /posts
  def create
    if current_user.job_admin
      @post = Post.create!(user: current_user, title: params[:title], description: params[:description])
      @post.save ? json_response(@post, :created) : json_response(@post.errors, :unprocessable_entity)
    else
      build_json_response(false, "ERROR, you must be an admin", :unprocessable_entity)
    end
  end

  # GET /posts/:id
  def show
    json_response(@post)
  end

  # PUT /posts/:id
  def update
    if current_user.job_admin
      @post.update(post_params)
      @post.save ? json_response(@post) : json_response(@post.errors, :unprocessable_entity)
    else
      build_json_response(false, "ERROR, you must be an admin", :unprocessable_entity)
    end
  end

  # DELETE /posts/:id
  def destroy
    #@post = Post.find(params[:id])
    if current_user.job_admin && @post.destroy
      head :no_content
    else
      build_json_response(false, "ERROR, you must be an admin", :unprocessable_entity)
    end
  end

  private

  def post_params
    # whitelist params
    params.permit(:title, :description)
  end

  def set_post
    @post = Post.find(params[:id])
  end
end
