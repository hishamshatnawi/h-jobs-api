module Response
  def json_response(object, status = :ok)
    render json: object, status: status
  end

  def build_json_response(success, object, status = :ok)
    msg = {:success => success, :message => object}

    render json: msg, status: status
  end
end