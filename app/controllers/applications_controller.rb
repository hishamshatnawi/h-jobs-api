class ApplicationsController < ApplicationController
  include Response
  before_action :authenticate_request!
  before_action :set_user_application, only: [:destroy]

  before_action :set_application, only: [:show, :update, :destroy]
  before_action :set_post, only: [:create, :show, :update, :destroy]

  skip_before_action :verify_authenticity_token

  # GET /applications
  def index
    @applications = !current_user.job_admin ? Application.where(user: current_user) : Application.all
    if @applications.nil? || @applications.blank?
      msg = {:message => "There is no application record"}
      render :json => msg, :status => 201
      return
    end
    render json: @applications
  end

  # POST /applications
  def create
    if current_user.job_admin
      build_json_response(false, "Come on! you are the admin, you dont need to submit an apllication!", :unprocessable_entity)
      return
    end
    if @post.nil?
      build_json_response(false, "ERROR, Make sure you are applying for a valid JOB POST", :unprocessable_entity)
      return
    end
    @application = Application.create!(user: current_user, post: @post)
    @application.save ? json_response(@application, :created) : json_response(@application.errors, :unprocessable_entity)
  end

  # GET /applications/:id
  def show
    if current_user.job_admin && @application.status != 'Seen'
      @application.assign_attributes(status: 'Seen')
      @application.save ? json_response(@application) : json_response(@application.errors, :unprocessable_entity)
    else
      @application = Application.where(user: current_user, :id => params[:id])
      if @application.nil?
        build_json_response(false, "Make sure you are requesting a valid application", :unprocessable_entity)
        return
      end
      json_response(@application, :show)
    end
  end

  # PUT /applications/:id
  def update
    if current_user.job_admin && @application.status != 'Seen'
      @application.assign_attributes(status: 'Seen')
      @application.save ? json_response(@application) : json_response(@application.errors, :unprocessable_entity)
    end
    json_response(@application)
  end

  # DELETE /applications/:id
  def destroy

    if !current_user.job_admin && @application.destroy
      head :no_content
      build_json_response(true, "Job applications has been deleted", :unprocessable_entity)

    else
      build_json_response(false, "Admin is not allowed to delete job applications", :unprocessable_entity)
    end
  end

  private

  def application_params
    # whitelist params
    params.permit(:status)
  end

  def set_application
    @application = Application.find(params[:id])
  end

  def set_user_application
    @application = Application.where(user: current_user, :id => params[:id])
  end

  def set_post
    @post = Post.find(params[:post_id])
  end
end
