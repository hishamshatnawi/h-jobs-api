class HomeController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_request!

  def index
    render json: {'logged_in' => true}
  end
end
