require 'elasticsearch/model'
class Post < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :user
  has_many :applications, dependent: :destroy
  validates_presence_of :title, :description
end
Post.import