class Ability
  include CanCan::Ability

  def initialize(user)
    if user.present?  # logged in users (they can manage their posts)
      can :manage, Application, user_id: user.id
      can :read, Post # permissions for every logged user to read posts
      if user.job_admin?  # additional permissions for job administrator
        can :manage, :Post
        can :read, :Application
      end
    end
  end
end