class AddJobAdminToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :job_admin, :boolean, default: false
  end
end
