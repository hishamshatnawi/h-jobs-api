# README

* Ruby version : ruby 2.5.3p105 
* Rails version : Rails 5.2.2.1

* Func.:
	1- Admin can add/edit/delete posts
	2- Users can apply/edit/delete applications.
	3- Applications status will be "Seen" when Admin sees the applications.
	4- Login and Sign up 




* URLs to check:

             user_session POST   /users/sign_in(.:format)                                                                 users/sessions#create
                          POST   /users(.:format)                                                                         users/registrations#create
                auth_user POST   /auth_user(.:format)                                                                     authentication#authenticate_user
                     home GET    /home(.:format)                                                                          home#index
        post_applications GET    /posts/:post_id/applications(.:format)                                                   applications#index
                          POST   /posts/:post_id/applications(.:format)                                                   applications#create
         post_application GET    /posts/:post_id/applications/:id(.:format)                                               applications#show
                          PATCH  /posts/:post_id/applications/:id(.:format)                                               applications#update
                          PUT    /posts/:post_id/applications/:id(.:format)                                               applications#update
                          DELETE /posts/:post_id/applications/:id(.:format)                                               applications#destroy
                    posts GET    /posts(.:format)                                                                         posts#index
                          POST   /posts(.:format)                                                                         posts#create
                     post GET    /posts/:id(.:format)                                                                     posts#show
                          PATCH  /posts/:id(.:format)                                                                     posts#update
                          PUT    /posts/:id(.:format)                                                                     posts#update
                          DELETE /posts/:id(.:format)                                                                     posts#destroy

* NOTES: 
	1- Admin should be added by rails console
* Heroku Link:
	https://h-job-api.herokuapp.com/
	I'm stuck with TypeError (no implicit conversion of nil into String) error  app/lib/json_web_token.rb:4:in `encode' . even though I dont have this error on localhost