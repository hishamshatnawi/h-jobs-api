Rails.application.routes.draw do

  devise_for :users, path: 'users', controllers: {sessions: "users/sessions", registrations: "users/registrations"}
  devise_for :admins, path: 'admins', controllers: {sessions: "admins/sessions"}
  post 'auth_user' => 'authentication#authenticate_user'
  get 'home' => 'home#index'
  get 'search', to: 'search#search'
  resources :posts do
    resources :applications
  end
end
